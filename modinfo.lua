


--Beta, Final, or leave for no name
versiontype = ""
name = ""


author = "Clifford Walker"
avatar_icon = ""
--shadow, science or alchemy (Do not use any of it)


version = "1.0.0"




config = true
Language = "en"


tip = "󰀏The portals will automaticly switch󰀏"
tip_enabled = true



contributors = ""
write_contributors = false
credits_only = true



main_icon = "portal_pack"


priority = 9





api_version = 10

dst_compatible = true
dont_starve_compatible = false
reign_of_giants_compatible = false
shipwrecked_compatible = false


--valid items: yes, no, unknown or leave ""
compatible_caves = "unknown"


all_clients_require_mod = true
client_only_mod = false 
server_only_mod = false










local scales = {
}

for i = 1, 20 do
	scales[i] = {description = "x"..i/10, data = i/10}
end

local pos = {
	[1] = {description = "Default", data = 0}
}

for i = 2, 15 do
	pos[i] = {description = "+"..i.."0", data = i*10}
end

local opt_Empty = {{description = "", data = 0}}
local function Title(title,hover)
	return {
		name=title,
		hover=hover,
		options=opt_Empty,
		default=0,
	}
end


local SEPARATOR = Title("")





modinfo_ver = "2.0.3"
if config == true then


	--Config
	configuration_options =
	{
		Title("Portal Settings"),
		
		

		{
			name    = "portal",
			label   = "Portal",
			hover   = "",
			options =
			
			{
				{description = "Default 󰀧", data = "multiplayer_portal"},
				{description = "Forge 󰀈", data = "lavaarena_portal"},
				{description = "Gorge 󰀋", data = "quagmire_portal"},

			},
			default = "multiplayer_portal",
		},

--[[
		{
			name    = "spawnt",
			label   = "Portal Music",
			hover   = "Do you want there to be music at start",
			options =
			
			{
				{description = "Yes", data = 12, hover = "Note: There will be long delay"},
				{description = "No", data = 2},


			},
			default = 12,
		},--]]
	
			


			Title("󰀔 Mod Version"..":".." "..version),
			Title("󰀂 Modinfo Version:".." "..modinfo_ver),
				
	}
		
end


































icon_atlas = main_icon..".xml" or ""
icon = main_icon..".tex" or ""


versiontypes = {
    final = "[Final]",
    first = "[First Release]",
    


    disc = "[Discontinued]",
    

    refresh = "[Refresh]",
    redux = "[Redux]",
    
    --Early
    beta = "[Beta]",
    ea = "[Early Access]",
    hot = "[Hot Fix]",

}
versiontype = versiontypes[versiontype] or ""



modinfo_ver = modinfo_ver

if not versiontype  then
	name = name
else
	name = name.." \n"..versiontype..""
end



icons = 
{
	science = " 󰀔",
	alchemy = " 󰀝",
	shadow = " 󰀩",
}


avatar_icon = icons[avatar_icon] or ""


if tip_enabled == false then
	tip = ""
else
	tip = "\n"..tip
end



old_author = author
if contributors == "" or  contributors == nil  then 
	author = author..""..avatar_icon

elseif write_contributors == true then
	author = author.." and ".." "..contributors
end






if Language == "en" or Language == nil or Language == "" then
	--Description Components
	desc = [[This mod adds two new portals. Gorge and Forge, you have to select one via the mod config. Have fun!]]
	changelog= [[󰀏 What's New:
	󰀈 Option to switch portals
	󰀈 Added two portals
	󰀈 Added resurrection animation for Endless
	󰀈 Made the portal remove skeletons
]]
	




	copyright = "Copyright © 2019 "..old_author
	credits = "󰀭 Credits:".." "..contributors
	mark2 = "󰀂 Modinfo Version:".." "..modinfo_ver


end








caves_compatibility = 
{

	yes = "Yes, is compatible",
	mo = "No, is not compatible",
	unknown = "Unknown",

}



compatible_caves = caves_compatibility[compatible_caves] or ""
if compatible_caves ~= "" and client_only_mod == false then
	compatible_caves = "󰀗Works with Caves Status: "..compatible_caves
elseif compatible_caves ~= "" and client_only_mod == true then
	compatible_caves = ""
end




if write_contributors == true or credits_only == true and contributors ~= ""  then
	descfill = desc.."\n\n󰀝 Mod Version: "..version.."\n"..changelog..""..tip.."\n\n"..compatible_caves.."\n󰀀"..copyright.."\n\n"..credits.."\n\n"..mark2
elseif write_contributors == false or write_contributors == nil or credits_only == false or credits_only == nil and contributors == nil or contributors == "" then
	descfill = desc.."\n\n󰀝 Mod Version: "..version.."\n"..changelog..""..tip.."\n\n"..compatible_caves.."\n󰀀"..copyright.."\n\n"..mark2
end


description = descfill
description = description
