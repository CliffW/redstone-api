








function MysteryBox(typeb, skin)
   









    AddClassPostConstruct("screens/redux/multiplayermainscreen", function(self,options, open_box_fn, completed_cb, control, down)
        


    local ItemBoxOpenerPopup = require "redstone_widgets/itemboxopenerpopup_modded"


















        
        local chests = 
        {
            "classic",
            "victorian",
            "nature",
            "ice",
            "forge",
            "heart",
            "forge_all",
            "hallowed",

        }




   

        local mysterybox = nil


        


        local random_skin = chests[math.random(1, #chests)]

       

        print("Random skin is "..random_skin)
        if  skin == "random"  then
            mysterybox = "box_"..typeb.."_"..random_skin
        else
            
            mysterybox = "box_"..typeb.."_"..skin

        end
        
   
        
        local update_number = 0

        
        local finished = false
        local boxes_ss = self.fixed_root:AddChild(Widget("boxes_ss"))
        
        self.open_box_fn = open_box_fn
        self.completed_cb = completed_cb


        self.bundle_icon = boxes_ss:AddChild(UIAnim())
        self.bundle_icon:SetScale(.3)
        self.bundle_icon:GetAnimState():PlayAnimation("idle", true)
        self.bundle_icon:GetAnimState():SetBuild(mysterybox)
        self.bundle_icon:GetAnimState():SetBank("box_shared")
        self.bundle_icon:SetPosition(500, 240)


       
        boxes_ss.open_btn = boxes_ss:AddChild(TEMPLATES.StandardButton())
        boxes_ss.open_btn:SetScale(0.5)
        boxes_ss.open_btn:SetText("What's New")
        boxes_ss.open_btn:SetPosition(500, 150)
        boxes_ss.open_btn:SetOnClick(function()
            update_number = 0

            finished = false
            self:Hide()    

       
        
            GLOBAL.TheFrontEnd:GetSound():SetVolume("FEMusic", 0)
            

            local options = {
                allow_cancel = true,
                box_build = mysterybox,
            }
            local item_types = 1
            
            self.box_popup = ItemBoxOpenerPopup(options, function(success_cb)
                 

                
                TheItems:OpenBox(1, function(success, item_types)
                    
                    
                    
                    PopupDialogScreen(STRINGS.UI.BOX_POPUP.SERVER_ERROR_TITLE, "hmm",
                    {
                        {
                            text = STRINGS.UI.BOX_POPUP.OK,
                            cb = function()
                                box_popup:_Close()
                            end
                        }
                    })
                    success_cb(item_types)
                end)
            end)
 
            
           
            self.box_popup.back_button:Hide()


            HEADERFONT = _G.HEADERFONT
            UICOLOURS = GLOBAL.UICOLOURS


            self.text = self.box_popup.center_root:AddChild(Text(HEADERFONT,40))
            self.text:SetPosition(8, 5 )
            self.text:SetColour(UICOLOURS.HIGHLIGHT_GOLD)
            self.text:EnableWordWrap(true)
            

            self.text.shadow = self.box_popup.center_root:AddChild(Text(HEADERFONT,40))
            self.text.shadow:SetPosition(10, 5 )
            self.text.shadow:SetColour(0,0,0, 1)
            self.text.shadow:EnableWordWrap(true)
            
            self.text:MoveToFront()


            self.text:SetString([[
            󰀈 Fixed Issue with automatically enabling mod
            
            󰀈 Visually Updated the Missing Mod Checker
            ]])
            self.text:Hide()
                            


            local RARITY_SOUND = {
                "dontstarve/HUD/Together_HUD/collectionscreen/music/1_lootbox_common",
                "dontstarve/HUD/Together_HUD/collectionscreen/music/2_lootbox_classy",
                "dontstarve/HUD/Together_HUD/collectionscreen/music/3_lootbox_spiffy",
                "dontstarve/HUD/Together_HUD/collectionscreen/music/4_lootbox_distinguished",
                "dontstarve/HUD/Together_HUD/collectionscreen/music/5_lootbox_elegant",
            }


            self.box_popup.bundle:GetAnimState():HideSymbol("SWAP_ICON")
            self.inst:DoTaskInTime(2, function()
                
            end)


            local updates = 
            {
                "Menu Pack \n Changes Menu according to mod settings",
                "Added Sudura's VR headset",


            }
            

            local current_time = #updates - .25
            

            if finished == false then
            
                self.inst:DoPeriodicTask(current_time, function()
                    

                    if update_number == #updates and (self.box_popup.bundle:GetAnimState():IsCurrentAnimation("skin_loop", true) or self.box_popup.bundle:GetAnimState():IsCurrentAnimation("idle", true)) then
                        self.box_popup:_Close()
                        update_number = 4
                        self.box_popup:Kill()
                        self:Show()
                        finished = true
                        
                        
                    end



                    if  update_number ~= #updates and finished == false and (self.box_popup.bundle:GetAnimState():IsCurrentAnimation("skin_loop", true) or self.box_popup.bundle:GetAnimState():IsCurrentAnimation("idle", true)) then
                        self.text:Show()
                        GLOBAL.TheFrontEnd:GetSound():PlaySound( RARITY_SOUND[math.random(1, #RARITY_SOUND)] )
                        self.box_popup:_RevealNextItem()

                        self.text.shadow:SetString(updates[update_number + 1])
                        self.text:SetString(updates[update_number + 1])
                        update_number = update_number + 1
                   

                    end
                end)
            end






            self.inst:DoPeriodicTask(1/30, function()
                if not self.box_popup.bundle:GetAnimState():IsCurrentAnimation("skin_loop", true) or update_number == #updates + 1 then
                    self.text:Hide()
                    self.text.shadow:Hide()
                else
                    self.text:Show()
                    self.text.shadow:Show()
                end

            end)






     --[[      self.inst:DoTaskInTime(6.50, function()
                self.box_popup:_Close()
                self.text:Hide()
                boxes_ss.open_btn:Show()
            end)--]]






   


            self.back_button = self.box_popup.bundle_root:AddChild(TEMPLATES.BackButton(
                function() self.box_popup:_Close() self.back_button:Kill() end
            ))
            self.back_button:MoveToFront()

            self.default_focus = self.box_popup.bundle_root
            self.box_popup.bundle_root:SetFocus()
            self.box_popup:MoveToFront()
        end)
        



    
        
    end)



















end
                            

           